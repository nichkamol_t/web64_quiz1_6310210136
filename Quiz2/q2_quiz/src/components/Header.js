import {Link} from "react-router-dom";

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';


function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant = "h7">ยินดีต้อนรับสู่เว็บไซต์ของฉัน :  &nbsp;&nbsp;</Typography>
         
         <Link to="/"> 
              <Typography variant = "b2">
              ดูดวง 
               </Typography>
         </Link>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <Link to="/about"> 
            <Typography variant = "b2"> 
             ผู้จัดทำเว็บ
             </Typography>
         </Link>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
export default Header;

