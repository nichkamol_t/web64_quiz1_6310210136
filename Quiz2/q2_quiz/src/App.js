import logo from './logo.svg';
import './App.css';

import AboutMePage from './Page/AboutMePage';
import ForturnPage from './Page/ForturnPage';
import Header from './components/Header';

import { Routes, Route, Link } from "react-router-dom";

function App() {
  return (
    <div className="App">
              <Header/>
       <Routes>
             <Route path="about" element = {
               <AboutMePage/>
             }/>

        <Route path="/" element={
               <ForturnPage/> 
               }/>

      </Routes>
    </div>
  );
}

export default App;
